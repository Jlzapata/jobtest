import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CoreAppComponent } from './core-app/core-app.component';
import {formComponent} from './form/form.component';


const routes: Routes = [
    {path:'form', component:formComponent},
    { path: 'core', component: CoreAppComponent },
    { path: '**', redirectTo: '/core' }
];

@NgModule({
    imports: [RouterModule.forRoot(
        routes,
         { enableTracing: false }
    )],
    exports: [RouterModule]
})
export class AppRoutingModule { }
