import { Component, OnInit, Host } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import {CurrentWeather} from './weather/Weather'
import { observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-core',
  templateUrl:'./core-app.component.html',
  styleUrls: ['./core-app.component.css']
})
export class CoreAppComponent implements OnInit {

  title:string;
  subtitle:string;
  currentPrimeNumber:number;
  openWeatherComponent: boolean = false;
  private timer;
  public bodyBackgroundColor:string;
  private knownPrimeNumbers:Array<number>;
  constructor(public http:HttpClient) { }

  ngOnInit() {
    this.title = 'Julian Zapata';
    this.subtitle = 'jl.zapata0@gmail.com';
    this.currentPrimeNumber = 1;
    this.knownPrimeNumbers = [];
    this.bodyBackgroundColor ='rgba(0,0,0,1)';
  }



  changeBodyBackgroundColor() {
    const colorComponents: Array<number> = [0, 0, 0, 1];
    colorComponents.forEach((element, index, array) => {
      if (index !== 3) {
        array[index] = Math.floor(Math.random() * 255);
      } else {
        array[index] = Math.random();
       }
    });
    
    document.querySelector('body').style.backgroundColor = `rgba(${colorComponents[0]},${colorComponents[1]},${colorComponents[2]},${colorComponents[3]})`;
  }

  sendToBackInit() {
    if(this.timer == null)
      this.timer = setInterval(()=> {this.title = this.sendToBack(this.title)}, 500);
  }

  private sendToBack(expression:string):string{
    let lettersArray: Array<string> = expression.split('');
      const firstLetter = lettersArray.shift();
      lettersArray.push(firstLetter);
      return lettersArray.join('');
  }

  public getNextNumberPrime(){
      let numberTotest = this.currentPrimeNumber + 1;
      while(!this.numberIsPrime(numberTotest)){
        numberTotest += 1;
      }

      this.knownPrimeNumbers.push(numberTotest);
      this.currentPrimeNumber = numberTotest;
  }

  private numberIsPrime(number:number):boolean{
    let result:boolean;
      if(number == 2){
        return true;
      }

      const maxNumberToTest = Math.floor(Math.sqrt(number));
      if(this.knownPrimeNumbers.length > 0){
        for (let element of this.knownPrimeNumbers) {
          if(element > maxNumberToTest){
            return true;
          }else{
            if(number % element == 0){
              return false;
            }
          }
        }
        return this.primeBlindSearch(number, maxNumberToTest, this.knownPrimeNumbers[this.knownPrimeNumbers.length -1]);
      }

    return this.primeBlindSearch(number, maxNumberToTest, 2); 
  }


  private primeBlindSearch(numberTotest:number, maxNumberToTest: number, initialVal: number):boolean{
    while(maxNumberToTest <= initialVal){
      if(numberTotest % initialVal == 0){
        return false;
      }
        initialVal += 1;
    }

    return true;
  }


  // public getCurrentWeather(){
  //     this.http.get<CurrentWeather>('http://api.apixu.com/v1/current.json?key=2a8ee65c532e47b0b4b212655180411&q=Paris').subscribe(data=>{
  //       console.log(data.location.country);
  //     });
  // }
}
