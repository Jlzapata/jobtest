
export interface Condition{
      text:string;
      icon:string;
}


export interface Weather{
     temp_c:number;
     wind_kmh:number;
     wind_dir:string;
     precip_in:number;
     uv:number;
     pressure:number;
     condition:Condition; 
}


export interface Location{
     name:string;
     country:string;
     lat:number;
     lon:number;


}


export interface CurrentWeather{
     location:Location;
     current:Weather;

}
