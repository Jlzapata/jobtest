import { Component, OnInit, Input } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import{ CurrentWeather, Location, Weather} from './Weather';
import { Event } from '@angular/router';

@Component({
  selector: 'app-weather',
  templateUrl:'./weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  city = new FormControl('',Validators.required);
  submittedForm:boolean = false;
  currentWeather:CurrentWeather;
  private url:string;
  private appKey:string;

  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.url = "http://api.apixu.com/v1/current.json?";
    this.appKey = "2a8ee65c532e47b0b4b212655180411";
  }

  public onSubmit(event:Event){
      if(this.city.errors){
        return;
      }

      this.http.get<CurrentWeather>(`${this.url}key=${this.appKey}&q=${this.city.value}`)
      .subscribe(
        data=> {
          this.currentWeather = data;
          this.submittedForm = true;
        },
        error => {
          if(error.status === 400){
            alert("Ciudad no encontrada!");
          }else{
            alert("Error en el servidor!");
          }
          
        });
  }

  public hideInfo(){
    this.submittedForm = false;
  }

}
