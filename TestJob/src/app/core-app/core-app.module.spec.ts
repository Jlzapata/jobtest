import { CoreAppModule } from './core-app.module';

describe('CoreAppModule', () => {
  let coreAppModule: CoreAppModule;

  beforeEach(() => {
    coreAppModule = new CoreAppModule();
  });

  it('should create an instance', () => {
    expect(coreAppModule).toBeTruthy();
  });
});
