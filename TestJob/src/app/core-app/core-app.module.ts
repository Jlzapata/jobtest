import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreAppComponent } from './core-app.component';
import{RouterModule} from '@angular/router';
import { WeatherComponent } from './weather/weather.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    RouterModule
  ],
  declarations: [CoreAppComponent, WeatherComponent]
})
export class CoreAppModule { }


