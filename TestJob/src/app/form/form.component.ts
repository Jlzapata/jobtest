import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-form',
  templateUrl:'./form.component.html',
  styleUrls:['./form.component.css']
})
export class formComponent implements OnInit {

  public userForm:FormGroup;

  constructor(public ngxSmartModalService:NgxSmartModalService,private fb:FormBuilder) { 
  
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      name: ['',Validators.required],
      age: ['',Validators.required],
      color:['']
    });
  }

  get f() { return this.userForm.controls; }



  onSubmit(){

    // stop here if form is invalid
    if (this.userForm.invalid) {
        return;
    }

    this.ngxSmartModalService.getModal('myModal').open()
  }

}
