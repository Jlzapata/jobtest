import { Component } from '@angular/core';

@Component({
  selector: 'abe-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'App-Test';
}
